﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace auta
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        string jmenoSouboru;
        string barva;
        int index;
        string obrazek;
        StreamWriter pridat;

        private void btExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btNacist_Click(object sender, EventArgs e)
        {
            DialogResult odpoved = otevreniSouboru.ShowDialog();
            if (odpoved == DialogResult.OK)
            {
                jmenoSouboru = otevreniSouboru.FileName;
            }
            tbCesta.Text = jmenoSouboru;

            LBVystup.Items.Clear();
            StreamReader soubor = new StreamReader(jmenoSouboru, Encoding.Default);
            string radek = null;
            radek = soubor.ReadLine();
          

            while ((radek = soubor.ReadLine()) != null)
            {
                string[] hodnoty = radek.Split(';');
                string jmeno = hodnoty[6];
                string znacka = hodnoty[0];
                     


                LBVystup.Items.Add(znacka + "\t" + jmeno);


         
             

                 
        
            }
            soubor.Close();

        }

        private void LBVystup_MouseClick(object sender, MouseEventArgs e)
        {
            index = LBVystup.SelectedIndex;
            int poloha = index + 1;
            StreamReader soubor = new StreamReader(jmenoSouboru, Encoding.Default);
            string radek = null;
            radek = soubor.ReadLine();
            for (int i = 0; i < poloha; i++)
            {
                radek = soubor.ReadLine();
            }

                string[] hodnoty = radek.Split(';');
                string jmeno = hodnoty[6];
                string znacka = hodnoty[0];
                string barva = hodnoty[1];
                string rok = hodnoty[2];
                string typ = hodnoty[3];
                string vybava = hodnoty[4];
                 obrazek = hodnoty[5];
                string spz = hodnoty[7];
                soubor.Close();

                tbBarva.Text = barva;
                tbFoto.Text = obrazek;
                tbMajitel.Text = jmeno;
                tbRok.Text = rok;
                tbSPZ.Text = spz;
                tbTyp.Text = typ;
                tbVybava.Text = vybava;
                tbZnacka.Text = znacka;



                pb.Image = Image.FromFile("img\\"+obrazek);

        }

        private void pb_Click(object sender, EventArgs e)
        {

        }

        private void btUpravit_Click(object sender, EventArgs e)
        {


           pridat = new StreamWriter(jmenoSouboru, true, Encoding.Default);
           string barva = tbBarva.Text;
          string  obrazek = tbFoto.Text;
           string jmeno=tbMajitel.Text;
           string rok = tbRok.Text;
          string  spz = tbSPZ.Text;
           string typ = tbTyp.Text;
          string  vybava = tbVybava.Text;
          string  znacka =tbZnacka.Text;
        
            string zapis = znacka + ";" + barva + ";" + rok + ";" + typ + ";" + vybava + ";" + obrazek + ";" + jmeno + ";" + spz;
       
          
            pridat.Close();
            
        }

        private void btPridat_Click(object sender, EventArgs e)
        {
            StreamWriter pridat = new StreamWriter(jmenoSouboru, true,Encoding.Default);


           string barva = tbBarva.Text;
           string obrazek = tbFoto.Text;
           string jmeno = tbMajitel.Text;
           string rok = tbRok.Text;
           string spz = tbSPZ.Text;
           string typ = tbTyp.Text;
           string vybava = tbVybava.Text;
           string znacka = tbZnacka.Text;

           string zapis = znacka + ";" + barva + ";" + rok + ";" + typ + ";" + vybava + ";" + obrazek + ";" + jmeno + ";" + spz;
           LBVystup.Items.Add(znacka + "\t" + jmeno); 
           pridat.WriteLine(zapis);
           pridat.Close();
           MessageBox.Show("Zápis přidán!");








        }

        private void btRefresh_Click(object sender, EventArgs e)
        {
            StreamReader soubor = new StreamReader(jmenoSouboru, Encoding.Default);
            string radek = null;
            radek = soubor.ReadLine();

            LBVystup.Items.Clear();
            while ((radek = soubor.ReadLine()) != null)
            {
                string[] hodnoty = radek.Split(';');
                string jmeno = hodnoty[6];
                string znacka = hodnoty[0];



                LBVystup.Items.Add(znacka + "\t" + jmeno);







            }
            soubor.Close();
        }

      
    }
}
