﻿namespace auta
{
    partial class Form1
    {
        /// <summary>
        /// Vyžadovaná proměnná návrháře.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Uvolnit všechny používané prostředky.
        /// </summary>
        /// <param name="disposing">hodnota true, když by měl být spravovaný prostředek odstraněn, jinak false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kód generovaný Návrhářem Windows Form

        /// <summary>
        /// Metoda vyžadovaná pro podporu návrháře - neupravovat
        /// obsah této metody v editoru kódu.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbCesta = new System.Windows.Forms.TextBox();
            this.btNacist = new System.Windows.Forms.Button();
            this.btExit = new System.Windows.Forms.Button();
            this.LBVystup = new System.Windows.Forms.ListBox();
            this.tbTyp = new System.Windows.Forms.TextBox();
            this.tbSPZ = new System.Windows.Forms.TextBox();
            this.tbRok = new System.Windows.Forms.TextBox();
            this.tbMajitel = new System.Windows.Forms.TextBox();
            this.tbBarva = new System.Windows.Forms.TextBox();
            this.tbFoto = new System.Windows.Forms.TextBox();
            this.tbZnacka = new System.Windows.Forms.TextBox();
            this.tbVybava = new System.Windows.Forms.TextBox();
            this.Značka = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.otevreniSouboru = new System.Windows.Forms.OpenFileDialog();
            this.pb = new System.Windows.Forms.PictureBox();
            this.btUpravit = new System.Windows.Forms.Button();
            this.btPridat = new System.Windows.Forms.Button();
            this.btRefresh = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pb)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(313, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Registr vozidel";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Procházet:";
            // 
            // tbCesta
            // 
            this.tbCesta.Location = new System.Drawing.Point(87, 72);
            this.tbCesta.Name = "tbCesta";
            this.tbCesta.Size = new System.Drawing.Size(100, 20);
            this.tbCesta.TabIndex = 2;
            // 
            // btNacist
            // 
            this.btNacist.Location = new System.Drawing.Point(206, 71);
            this.btNacist.Name = "btNacist";
            this.btNacist.Size = new System.Drawing.Size(75, 23);
            this.btNacist.TabIndex = 3;
            this.btNacist.Text = "Načíst";
            this.btNacist.UseVisualStyleBackColor = true;
            this.btNacist.Click += new System.EventHandler(this.btNacist_Click);
            // 
            // btExit
            // 
            this.btExit.Location = new System.Drawing.Point(606, 337);
            this.btExit.Name = "btExit";
            this.btExit.Size = new System.Drawing.Size(75, 23);
            this.btExit.TabIndex = 4;
            this.btExit.Text = "Ukončit";
            this.btExit.UseVisualStyleBackColor = true;
            this.btExit.Click += new System.EventHandler(this.btExit_Click);
            // 
            // LBVystup
            // 
            this.LBVystup.FormattingEnabled = true;
            this.LBVystup.Location = new System.Drawing.Point(26, 115);
            this.LBVystup.Name = "LBVystup";
            this.LBVystup.Size = new System.Drawing.Size(267, 225);
            this.LBVystup.TabIndex = 5;
            this.LBVystup.MouseClick += new System.Windows.Forms.MouseEventHandler(this.LBVystup_MouseClick);
            // 
            // tbTyp
            // 
            this.tbTyp.Location = new System.Drawing.Point(404, 139);
            this.tbTyp.Name = "tbTyp";
            this.tbTyp.Size = new System.Drawing.Size(100, 20);
            this.tbTyp.TabIndex = 7;
            // 
            // tbSPZ
            // 
            this.tbSPZ.Location = new System.Drawing.Point(576, 138);
            this.tbSPZ.Name = "tbSPZ";
            this.tbSPZ.Size = new System.Drawing.Size(100, 20);
            this.tbSPZ.TabIndex = 8;
            // 
            // tbRok
            // 
            this.tbRok.Location = new System.Drawing.Point(404, 107);
            this.tbRok.Name = "tbRok";
            this.tbRok.Size = new System.Drawing.Size(100, 20);
            this.tbRok.TabIndex = 9;
            // 
            // tbMajitel
            // 
            this.tbMajitel.Location = new System.Drawing.Point(576, 107);
            this.tbMajitel.Name = "tbMajitel";
            this.tbMajitel.Size = new System.Drawing.Size(100, 20);
            this.tbMajitel.TabIndex = 10;
            // 
            // tbBarva
            // 
            this.tbBarva.Location = new System.Drawing.Point(404, 79);
            this.tbBarva.Name = "tbBarva";
            this.tbBarva.Size = new System.Drawing.Size(100, 20);
            this.tbBarva.TabIndex = 11;
            // 
            // tbFoto
            // 
            this.tbFoto.Location = new System.Drawing.Point(576, 76);
            this.tbFoto.Name = "tbFoto";
            this.tbFoto.Size = new System.Drawing.Size(100, 20);
            this.tbFoto.TabIndex = 12;
            // 
            // tbZnacka
            // 
            this.tbZnacka.Location = new System.Drawing.Point(404, 53);
            this.tbZnacka.Name = "tbZnacka";
            this.tbZnacka.Size = new System.Drawing.Size(100, 20);
            this.tbZnacka.TabIndex = 13;
            // 
            // tbVybava
            // 
            this.tbVybava.Location = new System.Drawing.Point(576, 53);
            this.tbVybava.Name = "tbVybava";
            this.tbVybava.Size = new System.Drawing.Size(100, 20);
            this.tbVybava.TabIndex = 14;
            // 
            // Značka
            // 
            this.Značka.AutoSize = true;
            this.Značka.Location = new System.Drawing.Point(352, 53);
            this.Značka.Name = "Značka";
            this.Značka.Size = new System.Drawing.Size(44, 13);
            this.Značka.TabIndex = 15;
            this.Značka.Text = "Značka";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(355, 78);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Barva";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(335, 107);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Rok výroby";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(355, 138);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(25, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Typ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(535, 53);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "Výbava";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(538, 76);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(28, 13);
            this.label8.TabIndex = 20;
            this.label8.Text = "Foto";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(538, 107);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 13);
            this.label9.TabIndex = 21;
            this.label9.Text = "Majitel";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(538, 138);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(28, 13);
            this.label10.TabIndex = 22;
            this.label10.Text = "SPZ";
            // 
            // otevreniSouboru
            // 
            this.otevreniSouboru.FileName = "openFileDialog1";
            // 
            // pb
            // 
            this.pb.Location = new System.Drawing.Point(358, 165);
            this.pb.Name = "pb";
            this.pb.Size = new System.Drawing.Size(318, 152);
            this.pb.TabIndex = 23;
            this.pb.TabStop = false;
            this.pb.Click += new System.EventHandler(this.pb_Click);
            // 
            // btUpravit
            // 
            this.btUpravit.Location = new System.Drawing.Point(479, 337);
            this.btUpravit.Name = "btUpravit";
            this.btUpravit.Size = new System.Drawing.Size(75, 23);
            this.btUpravit.TabIndex = 24;
            this.btUpravit.Text = "Upravit";
            this.btUpravit.UseVisualStyleBackColor = true;
            this.btUpravit.Click += new System.EventHandler(this.btUpravit_Click);
            // 
            // btPridat
            // 
            this.btPridat.Location = new System.Drawing.Point(358, 337);
            this.btPridat.Name = "btPridat";
            this.btPridat.Size = new System.Drawing.Size(75, 23);
            this.btPridat.TabIndex = 25;
            this.btPridat.Text = "Přidat";
            this.btPridat.UseVisualStyleBackColor = true;
            this.btPridat.Click += new System.EventHandler(this.btPridat_Click);
            // 
            // btRefresh
            // 
            this.btRefresh.Location = new System.Drawing.Point(218, 346);
            this.btRefresh.Name = "btRefresh";
            this.btRefresh.Size = new System.Drawing.Size(75, 23);
            this.btRefresh.TabIndex = 26;
            this.btRefresh.Text = "Refresh";
            this.btRefresh.UseVisualStyleBackColor = true;
            this.btRefresh.Click += new System.EventHandler(this.btRefresh_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(703, 372);
            this.Controls.Add(this.btRefresh);
            this.Controls.Add(this.btPridat);
            this.Controls.Add(this.btUpravit);
            this.Controls.Add(this.pb);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Značka);
            this.Controls.Add(this.tbVybava);
            this.Controls.Add(this.tbZnacka);
            this.Controls.Add(this.tbFoto);
            this.Controls.Add(this.tbBarva);
            this.Controls.Add(this.tbMajitel);
            this.Controls.Add(this.tbRok);
            this.Controls.Add(this.tbSPZ);
            this.Controls.Add(this.tbTyp);
            this.Controls.Add(this.LBVystup);
            this.Controls.Add(this.btExit);
            this.Controls.Add(this.btNacist);
            this.Controls.Add(this.tbCesta);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Databáze aut";
            ((System.ComponentModel.ISupportInitialize)(this.pb)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbCesta;
        private System.Windows.Forms.Button btNacist;
        private System.Windows.Forms.Button btExit;
        private System.Windows.Forms.ListBox LBVystup;
        private System.Windows.Forms.TextBox tbTyp;
        private System.Windows.Forms.TextBox tbSPZ;
        private System.Windows.Forms.TextBox tbRok;
        private System.Windows.Forms.TextBox tbMajitel;
        private System.Windows.Forms.TextBox tbBarva;
        private System.Windows.Forms.TextBox tbFoto;
        private System.Windows.Forms.TextBox tbZnacka;
        private System.Windows.Forms.TextBox tbVybava;
        private System.Windows.Forms.Label Značka;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.OpenFileDialog otevreniSouboru;
        private System.Windows.Forms.PictureBox pb;
        private System.Windows.Forms.Button btUpravit;
        private System.Windows.Forms.Button btPridat;
        private System.Windows.Forms.Button btRefresh;
    }
}

