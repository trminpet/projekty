﻿namespace barvy
{
    partial class oknoProgramu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.popisekBarva = new System.Windows.Forms.Label();
            this.popisekR = new System.Windows.Forms.Label();
            this.popisekG = new System.Windows.Forms.Label();
            this.popisekMichana = new System.Windows.Forms.Label();
            this.popisekRm = new System.Windows.Forms.Label();
            this.popisekGm = new System.Windows.Forms.Label();
            this.popisekB = new System.Windows.Forms.Label();
            this.popisekBm = new System.Windows.Forms.Label();
            this.poleR = new System.Windows.Forms.TextBox();
            this.poleG = new System.Windows.Forms.TextBox();
            this.poleB = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.panelZluta = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // popisekBarva
            // 
            this.popisekBarva.AutoSize = true;
            this.popisekBarva.Location = new System.Drawing.Point(13, 13);
            this.popisekBarva.Name = "popisekBarva";
            this.popisekBarva.Size = new System.Drawing.Size(61, 13);
            this.popisekBarva.TabIndex = 0;
            this.popisekBarva.Text = "Žlutá barva";
            // 
            // popisekR
            // 
            this.popisekR.AutoSize = true;
            this.popisekR.Location = new System.Drawing.Point(16, 53);
            this.popisekR.Name = "popisekR";
            this.popisekR.Size = new System.Drawing.Size(18, 13);
            this.popisekR.TabIndex = 1;
            this.popisekR.Text = "R:";
            // 
            // popisekG
            // 
            this.popisekG.AutoSize = true;
            this.popisekG.Location = new System.Drawing.Point(16, 90);
            this.popisekG.Name = "popisekG";
            this.popisekG.Size = new System.Drawing.Size(18, 13);
            this.popisekG.TabIndex = 2;
            this.popisekG.Text = "G:";
            // 
            // popisekMichana
            // 
            this.popisekMichana.AutoSize = true;
            this.popisekMichana.Location = new System.Drawing.Point(16, 274);
            this.popisekMichana.Name = "popisekMichana";
            this.popisekMichana.Size = new System.Drawing.Size(80, 13);
            this.popisekMichana.TabIndex = 3;
            this.popisekMichana.Text = "Míchaná barva";
            // 
            // popisekRm
            // 
            this.popisekRm.AutoSize = true;
            this.popisekRm.Location = new System.Drawing.Point(19, 307);
            this.popisekRm.Name = "popisekRm";
            this.popisekRm.Size = new System.Drawing.Size(18, 13);
            this.popisekRm.TabIndex = 4;
            this.popisekRm.Text = "R:";
            // 
            // popisekGm
            // 
            this.popisekGm.AutoSize = true;
            this.popisekGm.Location = new System.Drawing.Point(19, 350);
            this.popisekGm.Name = "popisekGm";
            this.popisekGm.Size = new System.Drawing.Size(18, 13);
            this.popisekGm.TabIndex = 5;
            this.popisekGm.Text = "G:";
            // 
            // popisekB
            // 
            this.popisekB.AutoSize = true;
            this.popisekB.Location = new System.Drawing.Point(22, 130);
            this.popisekB.Name = "popisekB";
            this.popisekB.Size = new System.Drawing.Size(17, 13);
            this.popisekB.TabIndex = 6;
            this.popisekB.Text = "B:";
            // 
            // popisekBm
            // 
            this.popisekBm.AutoSize = true;
            this.popisekBm.Location = new System.Drawing.Point(19, 408);
            this.popisekBm.Name = "popisekBm";
            this.popisekBm.Size = new System.Drawing.Size(17, 13);
            this.popisekBm.TabIndex = 7;
            this.popisekBm.Text = "B:";
            // 
            // poleR
            // 
            this.poleR.Location = new System.Drawing.Point(75, 53);
            this.poleR.Name = "poleR";
            this.poleR.Size = new System.Drawing.Size(100, 20);
            this.poleR.TabIndex = 8;
            // 
            // poleG
            // 
            this.poleG.Location = new System.Drawing.Point(75, 90);
            this.poleG.Name = "poleG";
            this.poleG.Size = new System.Drawing.Size(100, 20);
            this.poleG.TabIndex = 9;
            // 
            // poleB
            // 
            this.poleB.Location = new System.Drawing.Point(75, 130);
            this.poleB.Name = "poleB";
            this.poleB.Size = new System.Drawing.Size(100, 20);
            this.poleB.TabIndex = 10;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(61, 307);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(100, 20);
            this.textBox4.TabIndex = 11;
            this.textBox4.Text = "0";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(75, 350);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(100, 20);
            this.textBox5.TabIndex = 12;
            this.textBox5.Text = "0";
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(61, 408);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(100, 20);
            this.textBox6.TabIndex = 13;
            this.textBox6.Text = "0";
            // 
            // panelZluta
            // 
            this.panelZluta.Location = new System.Drawing.Point(196, 53);
            this.panelZluta.Name = "panelZluta";
            this.panelZluta.Size = new System.Drawing.Size(141, 171);
            this.panelZluta.TabIndex = 14;
            this.panelZluta.Paint += new System.Windows.Forms.PaintEventHandler(this.panelZluta_Paint);
            // 
            // panel2
            // 
            this.panel2.Location = new System.Drawing.Point(196, 274);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(141, 171);
            this.panel2.TabIndex = 15;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // oknoProgramu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(378, 482);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panelZluta);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.poleB);
            this.Controls.Add(this.poleG);
            this.Controls.Add(this.poleR);
            this.Controls.Add(this.popisekBm);
            this.Controls.Add(this.popisekB);
            this.Controls.Add(this.popisekGm);
            this.Controls.Add(this.popisekRm);
            this.Controls.Add(this.popisekMichana);
            this.Controls.Add(this.popisekG);
            this.Controls.Add(this.popisekR);
            this.Controls.Add(this.popisekBarva);
            this.Name = "oknoProgramu";
            this.Text = "Grafika";
            this.TextChanged += new System.EventHandler(this.oknoProgramu_TextChanged);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label popisekBarva;
        private System.Windows.Forms.Label popisekR;
        private System.Windows.Forms.Label popisekG;
        private System.Windows.Forms.Label popisekMichana;
        private System.Windows.Forms.Label popisekRm;
        private System.Windows.Forms.Label popisekGm;
        private System.Windows.Forms.Label popisekB;
        private System.Windows.Forms.Label popisekBm;
        private System.Windows.Forms.TextBox poleR;
        private System.Windows.Forms.TextBox poleG;
        private System.Windows.Forms.TextBox poleB;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Panel panelZluta;
        private System.Windows.Forms.Panel panel2;
    }
}

