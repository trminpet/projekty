﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace barvy
{
    public partial class oknoProgramu : Form
    {
        public oknoProgramu()
        {
            InitializeComponent(); // Žlutou barvu do proměnné (autogenerující vlastnost)
            Color žlutáBarva = Color.Yellow;

            // Získání barevných komponent RGB dané barvy
            // (použití složek objektu Color)
            int R = žlutáBarva.R;
            int G = žlutáBarva.G;
            int B = žlutáBarva.B;

            // Zobrazení výsledků
            poleR.Text = Convert.ToString(R);
            poleG.Text = Convert.ToString(G);
            poleB.Text = Convert.ToString(B);
        }

        private void panelZluta_Paint(object sender, PaintEventArgs e)
        {
            Graphics kp = e.Graphics;
            Color žlutáBarva = Color.Yellow;
            Pen žlutéPero = new Pen(žlutáBarva);
            kp.DrawEllipse(žlutéPero, 0, 0, 70, 70);
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {
            Graphics kp = e.Graphics;

            try
            {
                // Hodnoty z textových polí do číselné formy
                int R = Convert.ToInt32(textBox4.Text);
                int G = Convert.ToInt32(textBox5.Text);
                int B = Convert.ToInt32(textBox6.Text);

                // Namíchání barvy (autogenerující metoda)
                Color Barva = Color.FromArgb(R, G, B);

                // Pero z míchané barvy (volání konstruktoru)
                Pen pero = new Pen(Barva);

                // Elipsa připraveným perem
                kp.DrawEllipse(pero, 0, 0, 70, 70);
            }
            catch
            {
                MessageBox.Show("ads");
            }
        }

        private void oknoProgramu_TextChanged(object sender, EventArgs e)
        {
            panel2.Refresh();
        }
    }
}
