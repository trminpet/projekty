﻿namespace trminek_test
{
    partial class oknoProgramu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tlacitkoOK = new System.Windows.Forms.Button();
            this.tlacitkoZnovu = new System.Windows.Forms.Button();
            this.tlacitkoKonec = new System.Windows.Forms.Button();
            this.popisekVtip = new System.Windows.Forms.Label();
            this.popisekHodnocení = new System.Windows.Forms.Label();
            this.popisekSeznam = new System.Windows.Forms.Label();
            this.poleVtip = new System.Windows.Forms.TextBox();
            this.poleHodnocení = new System.Windows.Forms.TextBox();
            this.poleSeznam = new System.Windows.Forms.TextBox();
            this.popisekČíslo = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tlacitkoOK
            // 
            this.tlacitkoOK.BackColor = System.Drawing.Color.Black;
            this.tlacitkoOK.ForeColor = System.Drawing.Color.PowderBlue;
            this.tlacitkoOK.Location = new System.Drawing.Point(75, 293);
            this.tlacitkoOK.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tlacitkoOK.Name = "tlacitkoOK";
            this.tlacitkoOK.Size = new System.Drawing.Size(100, 28);
            this.tlacitkoOK.TabIndex = 3;
            this.tlacitkoOK.Text = "OK";
            this.tlacitkoOK.UseVisualStyleBackColor = false;
            this.tlacitkoOK.Click += new System.EventHandler(this.tlacitkoOK_Click);
            // 
            // tlacitkoZnovu
            // 
            this.tlacitkoZnovu.BackColor = System.Drawing.Color.Black;
            this.tlacitkoZnovu.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.tlacitkoZnovu.ForeColor = System.Drawing.Color.PowderBlue;
            this.tlacitkoZnovu.Location = new System.Drawing.Point(300, 293);
            this.tlacitkoZnovu.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tlacitkoZnovu.Name = "tlacitkoZnovu";
            this.tlacitkoZnovu.Size = new System.Drawing.Size(100, 28);
            this.tlacitkoZnovu.TabIndex = 4;
            this.tlacitkoZnovu.Text = "Znovu";
            this.tlacitkoZnovu.UseVisualStyleBackColor = false;
            this.tlacitkoZnovu.Click += new System.EventHandler(this.tlacitkoZnovu_Click);
            // 
            // tlacitkoKonec
            // 
            this.tlacitkoKonec.BackColor = System.Drawing.Color.Black;
            this.tlacitkoKonec.ForeColor = System.Drawing.Color.PowderBlue;
            this.tlacitkoKonec.Location = new System.Drawing.Point(536, 293);
            this.tlacitkoKonec.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tlacitkoKonec.Name = "tlacitkoKonec";
            this.tlacitkoKonec.Size = new System.Drawing.Size(100, 28);
            this.tlacitkoKonec.TabIndex = 5;
            this.tlacitkoKonec.Text = "Konec";
            this.tlacitkoKonec.UseVisualStyleBackColor = false;
            this.tlacitkoKonec.Click += new System.EventHandler(this.tlacitkoKonec_Click);
            // 
            // popisekVtip
            // 
            this.popisekVtip.AutoSize = true;
            this.popisekVtip.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.popisekVtip.Location = new System.Drawing.Point(28, 25);
            this.popisekVtip.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.popisekVtip.Name = "popisekVtip";
            this.popisekVtip.Size = new System.Drawing.Size(39, 16);
            this.popisekVtip.TabIndex = 4;
            this.popisekVtip.Text = "Vtip:";
            // 
            // popisekHodnocení
            // 
            this.popisekHodnocení.AutoSize = true;
            this.popisekHodnocení.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.popisekHodnocení.Location = new System.Drawing.Point(28, 70);
            this.popisekHodnocení.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.popisekHodnocení.Name = "popisekHodnocení";
            this.popisekHodnocení.Size = new System.Drawing.Size(87, 16);
            this.popisekHodnocení.TabIndex = 4;
            this.popisekHodnocení.Text = "Hodnocení:";
            // 
            // popisekSeznam
            // 
            this.popisekSeznam.AutoSize = true;
            this.popisekSeznam.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.popisekSeznam.Location = new System.Drawing.Point(28, 128);
            this.popisekSeznam.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.popisekSeznam.Name = "popisekSeznam";
            this.popisekSeznam.Size = new System.Drawing.Size(104, 16);
            this.popisekSeznam.TabIndex = 5;
            this.popisekSeznam.Text = "Seznam vtipů:";
            // 
            // poleVtip
            // 
            this.poleVtip.Location = new System.Drawing.Point(140, 25);
            this.poleVtip.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.poleVtip.Name = "poleVtip";
            this.poleVtip.Size = new System.Drawing.Size(504, 22);
            this.poleVtip.TabIndex = 1;
            // 
            // poleHodnocení
            // 
            this.poleHodnocení.Location = new System.Drawing.Point(140, 70);
            this.poleHodnocení.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.poleHodnocení.Name = "poleHodnocení";
            this.poleHodnocení.Size = new System.Drawing.Size(35, 22);
            this.poleHodnocení.TabIndex = 2;
            // 
            // poleSeznam
            // 
            this.poleSeznam.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.poleSeznam.Location = new System.Drawing.Point(140, 128);
            this.poleSeznam.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.poleSeznam.Multiline = true;
            this.poleSeznam.Name = "poleSeznam";
            this.poleSeznam.ReadOnly = true;
            this.poleSeznam.Size = new System.Drawing.Size(504, 122);
            this.poleSeznam.TabIndex = 8;
            // 
            // popisekČíslo
            // 
            this.popisekČíslo.AutoSize = true;
            this.popisekČíslo.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.popisekČíslo.Location = new System.Drawing.Point(212, 73);
            this.popisekČíslo.Name = "popisekČíslo";
            this.popisekČíslo.Size = new System.Drawing.Size(188, 14);
            this.popisekČíslo.TabIndex = 0;
            this.popisekČíslo.Text = "Napiš hodnocení jako ve škole. (1-5)";
            // 
            // oknoProgramu
            // 
            this.AcceptButton = this.tlacitkoOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PowderBlue;
            this.CancelButton = this.tlacitkoZnovu;
            this.ClientSize = new System.Drawing.Size(679, 336);
            this.ControlBox = false;
            this.Controls.Add(this.popisekČíslo);
            this.Controls.Add(this.poleSeznam);
            this.Controls.Add(this.poleHodnocení);
            this.Controls.Add(this.poleVtip);
            this.Controls.Add(this.popisekSeznam);
            this.Controls.Add(this.popisekHodnocení);
            this.Controls.Add(this.popisekVtip);
            this.Controls.Add(this.tlacitkoKonec);
            this.Controls.Add(this.tlacitkoZnovu);
            this.Controls.Add(this.tlacitkoOK);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "oknoProgramu";
            this.Text = "Databáze vtipů";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button tlacitkoOK;
        private System.Windows.Forms.Button tlacitkoZnovu;
        private System.Windows.Forms.Button tlacitkoKonec;
        private System.Windows.Forms.Label popisekVtip;
        private System.Windows.Forms.Label popisekHodnocení;
        private System.Windows.Forms.Label popisekSeznam;
        private System.Windows.Forms.TextBox poleVtip;
        private System.Windows.Forms.TextBox poleHodnocení;
        private System.Windows.Forms.TextBox poleSeznam;
        private System.Windows.Forms.Label popisekČíslo;
    }
}

