﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace trminek_test
{
    public partial class oknoProgramu : Form
    {
        public oknoProgramu()
        {
            InitializeComponent();
        }

        private void tlacitkoKonec_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void tlacitkoZnovu_Click(object sender, EventArgs e)
        {
            poleHodnocení.Text = "";
            poleVtip.Text = "";
            poleSeznam.Text = "";
            
        }

        private void tlacitkoOK_Click(object sender, EventArgs e)
        {
            poleSeznam.Text += poleVtip.Text + " (" + poleHodnocení.Text + ")" + Environment.NewLine;
            poleHodnocení.Text = "";
            poleVtip.Text = "";
        }
    }
}
