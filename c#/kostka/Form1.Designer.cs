﻿namespace _07_kostka
{
    partial class oknoProgramu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tlacitkoHod = new System.Windows.Forms.Button();
            this.panel = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // tlacitkoHod
            // 
            this.tlacitkoHod.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tlacitkoHod.Location = new System.Drawing.Point(105, 330);
            this.tlacitkoHod.Name = "tlacitkoHod";
            this.tlacitkoHod.Size = new System.Drawing.Size(104, 37);
            this.tlacitkoHod.TabIndex = 0;
            this.tlacitkoHod.Text = "Házej!";
            this.tlacitkoHod.UseVisualStyleBackColor = true;
            this.tlacitkoHod.Click += new System.EventHandler(this.tlacitkoHod_Click);
            // 
            // panel
            // 
            this.panel.Location = new System.Drawing.Point(24, 29);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(274, 295);
            this.panel.TabIndex = 1;
            this.panel.Paint += new System.Windows.Forms.PaintEventHandler(this.panel_Paint);
            // 
            // oknoProgramu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(331, 379);
            this.Controls.Add(this.panel);
            this.Controls.Add(this.tlacitkoHod);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "oknoProgramu";
            this.Text = "Kostka";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button tlacitkoHod;
        private System.Windows.Forms.Panel panel;
    }
}

