﻿namespace _19_praceSdatumem
{
    partial class Form1
    {
        /// <summary>
        /// Vyžadovaná proměnná návrháře.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Uvolnit všechny používané prostředky.
        /// </summary>
        /// <param name="disposing">hodnota true, když by měl být spravovaný prostředek odstraněn, jinak false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kód generovaný Návrhářem Windows Form

        /// <summary>
        /// Metoda vyžadovaná pro podporu návrháře - neupravovat
        /// obsah této metody v editoru kódu.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btZpracuj = new System.Windows.Forms.Button();
            this.tbVstup = new System.Windows.Forms.TextBox();
            this.tbVystup = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(61, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Datum narození";
            // 
            // btZpracuj
            // 
            this.btZpracuj.Location = new System.Drawing.Point(64, 55);
            this.btZpracuj.Name = "btZpracuj";
            this.btZpracuj.Size = new System.Drawing.Size(75, 23);
            this.btZpracuj.TabIndex = 1;
            this.btZpracuj.Text = "Zpracuj";
            this.btZpracuj.UseVisualStyleBackColor = true;
            this.btZpracuj.Click += new System.EventHandler(this.btZpracuj_Click);
            // 
            // tbVstup
            // 
            this.tbVstup.Location = new System.Drawing.Point(55, 29);
            this.tbVstup.Name = "tbVstup";
            this.tbVstup.Size = new System.Drawing.Size(100, 20);
            this.tbVstup.TabIndex = 2;
            // 
            // tbVystup
            // 
            this.tbVystup.Location = new System.Drawing.Point(12, 84);
            this.tbVystup.Multiline = true;
            this.tbVystup.Name = "tbVystup";
            this.tbVystup.Size = new System.Drawing.Size(171, 116);
            this.tbVystup.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(195, 207);
            this.Controls.Add(this.tbVystup);
            this.Controls.Add(this.tbVstup);
            this.Controls.Add(this.btZpracuj);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Datum";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btZpracuj;
        private System.Windows.Forms.TextBox tbVstup;
        private System.Windows.Forms.TextBox tbVystup;
    }
}

