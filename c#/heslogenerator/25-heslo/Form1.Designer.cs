﻿namespace _25_heslo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btGeneruj = new System.Windows.Forms.Button();
            this.rbMala = new System.Windows.Forms.RadioButton();
            this.rbVelka = new System.Windows.Forms.RadioButton();
            this.rbVse = new System.Windows.Forms.RadioButton();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // btGeneruj
            // 
            this.btGeneruj.Location = new System.Drawing.Point(431, 194);
            this.btGeneruj.Name = "btGeneruj";
            this.btGeneruj.Size = new System.Drawing.Size(75, 23);
            this.btGeneruj.TabIndex = 0;
            this.btGeneruj.Text = "Generuj";
            this.btGeneruj.UseVisualStyleBackColor = true;
            this.btGeneruj.Click += new System.EventHandler(this.btGeneruj_Click);
            // 
            // rbMala
            // 
            this.rbMala.AutoSize = true;
            this.rbMala.Location = new System.Drawing.Point(99, 40);
            this.rbMala.Name = "rbMala";
            this.rbMala.Size = new System.Drawing.Size(134, 17);
            this.rbMala.TabIndex = 1;
            this.rbMala.TabStop = true;
            this.rbMala.Text = "malá písmena a číslice";
            this.rbMala.UseVisualStyleBackColor = true;
            // 
            // rbVelka
            // 
            this.rbVelka.AutoSize = true;
            this.rbVelka.Location = new System.Drawing.Point(99, 78);
            this.rbVelka.Name = "rbVelka";
            this.rbVelka.Size = new System.Drawing.Size(173, 17);
            this.rbVelka.TabIndex = 2;
            this.rbVelka.TabStop = true;
            this.rbVelka.Text = "malá písmena a velká písmena";
            this.rbVelka.UseVisualStyleBackColor = true;
            // 
            // rbVse
            // 
            this.rbVse.AutoSize = true;
            this.rbVse.Location = new System.Drawing.Point(99, 122);
            this.rbVse.Name = "rbVse";
            this.rbVse.Size = new System.Drawing.Size(98, 17);
            this.rbVse.TabIndex = 3;
            this.rbVse.TabStop = true;
            this.rbVse.Text = "Vše dohromady";
            this.rbVse.UseVisualStyleBackColor = true;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(287, 31);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(120, 186);
            this.listBox1.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(659, 264);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.rbVse);
            this.Controls.Add(this.rbVelka);
            this.Controls.Add(this.rbMala);
            this.Controls.Add(this.btGeneruj);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btGeneruj;
        private System.Windows.Forms.RadioButton rbMala;
        private System.Windows.Forms.RadioButton rbVelka;
        private System.Windows.Forms.RadioButton rbVse;
        private System.Windows.Forms.ListBox listBox1;
    }
}

