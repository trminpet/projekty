﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _24_LINQ_01
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string[] jmena;
        int[] poleCisel;
        bool[] poleStavu;
        private void btNacti_Click(object sender, EventArgs e)
        {
            // pole dat
            poleStavu = new bool[] { true, false, false, false, true, true, true, true };
            poleCisel = new int[] { 50, 8, -12, 17, -65, 1, 41, -28 };
            jmena = new string[] { "Petr", "Honza", "Tomas", "Petr", "Daniel","Dana" };
            /*
            for (int i = 0; i < jmena.Length; i++)
            {
                listBox1.Items.Add(jmena[i]);
            }
            */
            listBox1.Items.Clear();
            /*
            foreach (string j in jmena)
            {
                listBox1.Items.Add(j);
            }
             */
            foreach (int cislo in poleCisel)
            {
                listBox1.Items.Add(cislo);
            }



        }

        private void btSeradit_Click(object sender, EventArgs e)
        {
            //1.varianta
            /*
            IEnumerable<string> vyber = jmena.OrderBy(jm=>jm);
            listbox2.items.addrange(vyber.toArray());
             */
          //  IEnumerable<int> vyber = poleCisel.OrderBy(j => j);
            var vyber = poleCisel.Where(c => c > 0).OrderBy(c=>c);
            foreach (int cislo in vyber)
            {
                listBox2.Items.Add(cislo);
            }
            



        }

        private void btSeradit2_Click(object sender, EventArgs e)
        {
            //2.varianta
            // var vyber= jmena.OrderByDescending(j=>j);
            // listbox3.items.addrange(vyber.toArray());

          //  var vyber= poleCisel.OrderByDescending(c=> c >= 0);
            var vyber = poleCisel.Where(c => c <= 0).OrderByDescending(c=>c);
            
            
            foreach (int cislo in vyber)
            {
                listBox3.Items.Add(cislo);
            }



        }
        //metoda(funkce
        public string VybratJmena(string jm)
        {
            return jm = "Jmeno je:  " + jm;

        }

        private void btSetridit3_Click(object sender, EventArgs e)
        {
           // var vyber = jmena.Where(jmeno => jmeno == "Petr");
              var vyber = jmena.Where(jmeno => jmeno.Contains('a'));
           
                listBox4.Items.AddRange(vyber.ToArray());
            
        }
    }
}
