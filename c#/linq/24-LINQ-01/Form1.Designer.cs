﻿namespace _24_LINQ_01
{
    partial class Form1
    {
        /// <summary>
        /// Vyžadovaná proměnná návrháře.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Uvolnit všechny používané prostředky.
        /// </summary>
        /// <param name="disposing">hodnota true, když by měl být spravovaný prostředek odstraněn, jinak false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kód generovaný Návrhářem Windows Form

        /// <summary>
        /// Metoda vyžadovaná pro podporu návrháře - neupravovat
        /// obsah této metody v editoru kódu.
        /// </summary>
        private void InitializeComponent()
        {
            this.btNacti = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.btSeradit = new System.Windows.Forms.Button();
            this.btSeradit2 = new System.Windows.Forms.Button();
            this.listBox3 = new System.Windows.Forms.ListBox();
            this.btSetridit3 = new System.Windows.Forms.Button();
            this.listBox4 = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // btNacti
            // 
            this.btNacti.Location = new System.Drawing.Point(12, 30);
            this.btNacti.Name = "btNacti";
            this.btNacti.Size = new System.Drawing.Size(120, 23);
            this.btNacti.TabIndex = 0;
            this.btNacti.Text = "Načti";
            this.btNacti.UseVisualStyleBackColor = true;
            this.btNacti.Click += new System.EventHandler(this.btNacti_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(12, 62);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(120, 147);
            this.listBox1.TabIndex = 1;
            // 
            // listBox2
            // 
            this.listBox2.FormattingEnabled = true;
            this.listBox2.Location = new System.Drawing.Point(170, 62);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(120, 147);
            this.listBox2.TabIndex = 2;
            // 
            // btSeradit
            // 
            this.btSeradit.Location = new System.Drawing.Point(170, 30);
            this.btSeradit.Name = "btSeradit";
            this.btSeradit.Size = new System.Drawing.Size(120, 23);
            this.btSeradit.TabIndex = 3;
            this.btSeradit.Text = "Seřadit A-Z";
            this.btSeradit.UseVisualStyleBackColor = true;
            this.btSeradit.Click += new System.EventHandler(this.btSeradit_Click);
            // 
            // btSeradit2
            // 
            this.btSeradit2.Location = new System.Drawing.Point(327, 30);
            this.btSeradit2.Name = "btSeradit2";
            this.btSeradit2.Size = new System.Drawing.Size(120, 23);
            this.btSeradit2.TabIndex = 4;
            this.btSeradit2.Text = "Seřadit Z-A";
            this.btSeradit2.UseVisualStyleBackColor = true;
            this.btSeradit2.Click += new System.EventHandler(this.btSeradit2_Click);
            // 
            // listBox3
            // 
            this.listBox3.FormattingEnabled = true;
            this.listBox3.Location = new System.Drawing.Point(327, 62);
            this.listBox3.Name = "listBox3";
            this.listBox3.Size = new System.Drawing.Size(120, 147);
            this.listBox3.TabIndex = 5;
            // 
            // btSetridit3
            // 
            this.btSetridit3.Location = new System.Drawing.Point(502, 29);
            this.btSetridit3.Name = "btSetridit3";
            this.btSetridit3.Size = new System.Drawing.Size(120, 23);
            this.btSetridit3.TabIndex = 6;
            this.btSetridit3.Text = "Výběr";
            this.btSetridit3.UseVisualStyleBackColor = true;
            this.btSetridit3.Click += new System.EventHandler(this.btSetridit3_Click);
            // 
            // listBox4
            // 
            this.listBox4.FormattingEnabled = true;
            this.listBox4.Location = new System.Drawing.Point(502, 62);
            this.listBox4.Name = "listBox4";
            this.listBox4.Size = new System.Drawing.Size(120, 147);
            this.listBox4.TabIndex = 7;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 238);
            this.Controls.Add(this.listBox4);
            this.Controls.Add(this.btSetridit3);
            this.Controls.Add(this.listBox3);
            this.Controls.Add(this.btSeradit2);
            this.Controls.Add(this.btSeradit);
            this.Controls.Add(this.listBox2);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.btNacti);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btNacti;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.Button btSeradit;
        private System.Windows.Forms.Button btSeradit2;
        private System.Windows.Forms.ListBox listBox3;
        private System.Windows.Forms.Button btSetridit3;
        private System.Windows.Forms.ListBox listBox4;
    }
}

