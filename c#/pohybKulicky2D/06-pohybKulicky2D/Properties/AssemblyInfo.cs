﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Obecné informace o sestavení jsou řízeny prostřednictvím následující 
// sadu atributů. Změnou hodnot těchto atributů se upraví informace
// přidružené k sestavení.
[assembly: AssemblyTitle("06-pohybKulicky2D")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("06-pohybKulicky2D")]
[assembly: AssemblyCopyright("Copyright ©  2014")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Nastavení atributu ComVisible na hodnotu False udělá typy v tomto sestavení neviditelné 
// komponent modelu COM.  Pokud potřebujete přistoupit k typu v tomto sestavení z 
// modelu COM, nastavte atribut ComVisible daného typu na hodnotu True.
[assembly: ComVisible(false)]

// Následující GUID je použito pro ID knihovny typů, pokud je tento projekt vystaven COM
[assembly: Guid("fa33680b-44c9-4ca6-a847-c33efedb2dba")]

// Informace o verzi sestavení se skládá z následujících čtyř hodnot:
//
//      Hlavní verze
//      Dílčí verze 
//      Číslo sestavení
//      Revize
//
// Můžete zadat všechny hodnoty nebo nastavit výchozí očíslování sestavení a revize 
// použitím znaku '*' jak je ukázáno dále:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
