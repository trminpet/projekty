﻿namespace _06_pohybKulicky2D
{
    partial class Form1
    {
        /// <summary>
        /// Vyžadovaná proměnná návrháře.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Uvolnit všechny používané prostředky.
        /// </summary>
        /// <param name="disposing">hodnota true, když by měl být spravovaný prostředek odstraněn, jinak false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kód generovaný Návrhářem Windows Form

        /// <summary>
        /// Metoda vyžadovaná pro podporu návrháře - neupravovat
        /// obsah této metody v editoru kódu.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tlacitkoNovaKulicka = new System.Windows.Forms.Button();
            this.casovac = new System.Windows.Forms.Timer(this.components);
            this.skore = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tlacitkoNovaKulicka
            // 
            this.tlacitkoNovaKulicka.Location = new System.Drawing.Point(145, 342);
            this.tlacitkoNovaKulicka.Name = "tlacitkoNovaKulicka";
            this.tlacitkoNovaKulicka.Size = new System.Drawing.Size(116, 23);
            this.tlacitkoNovaKulicka.TabIndex = 0;
            this.tlacitkoNovaKulicka.Text = "Nová kulička";
            this.tlacitkoNovaKulicka.UseVisualStyleBackColor = true;
            this.tlacitkoNovaKulicka.Click += new System.EventHandler(this.tlacitkoNovaKulicka_Click);
            // 
            // casovac
            // 
            this.casovac.Tick += new System.EventHandler(this.casovac_Tick);
            // 
            // skore
            // 
            this.skore.AutoSize = true;
            this.skore.Location = new System.Drawing.Point(349, 342);
            this.skore.Name = "skore";
            this.skore.Size = new System.Drawing.Size(0, 13);
            this.skore.TabIndex = 1;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(422, 390);
            this.Controls.Add(this.skore);
            this.Controls.Add(this.tlacitkoNovaKulicka);
            this.DoubleBuffered = true;
            this.Name = "Form1";
            this.Text = "Kulička";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button tlacitkoNovaKulicka;
        private System.Windows.Forms.Timer casovac;
        private System.Windows.Forms.Label skore;
    }
}

