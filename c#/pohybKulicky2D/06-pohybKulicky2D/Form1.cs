﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _06_pohybKulicky2D
{
    public partial class Form1 : Form
    {
        int xkulicka, ykulicka;
        int polomer = 20;
        Random nahoda= new Random();
        int rychlostX, rychlostY;
        int x1 = 150;
        int x2 = 20;
        int x3 = 250;
        int y1 = 20;
        int y2 = 60;
        int y3 = 100;
        int sirka = 40;
        int vyska = 20;
        
       
        public Form1()
        {
            InitializeComponent();
        }
      

        private void Form1_Paint(object sender, PaintEventArgs e)
        {

           

           
            
            Graphics kp = e.Graphics;
            kp.FillEllipse(Brushes.MidnightBlue, xkulicka, ykulicka, polomer, polomer);
            kp.FillRectangle(Brushes.Black, x1, y1, sirka, vyska);
            kp.FillRectangle(Brushes.Red, x2, y2, sirka, vyska);
            kp.FillRectangle(Brushes.Lime, x3, y3, sirka, vyska);



           
        }

        private void tlacitkoNovaKulicka_Click(object sender, EventArgs e)
        {
            xkulicka = (Width / 2) - 20;
            ykulicka = (Height / 2) - 20;

            rychlostX = nahoda.Next(-80, 80);
            rychlostY = nahoda.Next(-80, 80);
            
          
            Refresh();
             casovac.Enabled = true;
 
        }

        private void casovac_Tick(object sender, EventArgs e)
        {
            double cas = casovac.Interval / 10000.0;
             
            xkulicka += Convert.ToInt32( (rychlostX * cas));
            ykulicka += Convert.ToInt32( (rychlostY * cas));
            //odraz od stěn
            if (xkulicka>=Width-40 || xkulicka<=0 )
            {
                rychlostX = -rychlostX;
            
               
               
              
                
            }

            if (ykulicka  >= Height-56 || ykulicka <= 0)
            {
                rychlostY = -rychlostY;


               
            }


            //odraz a zásah obdelníků
            if (xkulicka<x1+sirka && xkulicka+20>=x1 && ykulicka<y1+vyska && ykulicka+20>y1)
            {
                x1 = -500;
             if (xkulicka<x1+sirka && xkulicka+polomer>=x1)
	             {
                   rychlostX = -rychlostX;
	             }
                 
            if (ykulicka<y1+vyska && ykulicka+polomer > y1)
                {
                    
                    rychlostY = -rychlostY;


                }

 


         

           

            }

            if (xkulicka < x2 + sirka && xkulicka + 20 >= x2 && ykulicka < y2 + vyska && ykulicka + 20 > y2)
            {
                x2 = -500;
                if (xkulicka < x2 + sirka && xkulicka + polomer >= x2)
                {
                    rychlostX = -rychlostX;
                    
                }



                if (ykulicka < y2 + vyska && ykulicka + polomer > y2)
                {
                    rychlostY = -rychlostY;



                }
             
            }

            if (xkulicka < x3 + sirka && xkulicka + 20 >= x3 && ykulicka < y3 + vyska && ykulicka + 20 > y3)
            {
                x3 = -500;
                if (xkulicka < x3 + sirka && xkulicka + polomer >= x3)
                {
                    rychlostX = -rychlostX;
                }
                
                if (ykulicka<y3+vyska &&ykulicka+polomer >=y3)
                {
                    rychlostY = -rychlostY;
                }
                    

            }

         
            Refresh();
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            xkulicka = (Width / 2) - 20;
            ykulicka = (Height / 2) - 20;
        
        }

     
    }
}
