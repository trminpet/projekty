
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
 <head>
  <meta name="generator" content="PSPad editor, www.pspad.com" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />

  <title>Můj web</title>
   <link href="styl.css" rel="stylesheet" type="text/css" />
 </head>
 <style>
   h3
   {
   
      font-size:10pt;
      text-align: center;
       color:blue;
       margin-top:0px;
   
   }
   #do
   {
   color:red;
   }
 
 
 </style>
 <body>
    <div id="obal">
      
      <div id="nadpis">
       
        <h1>Vítejte na mé stránce </h1>  
       
      </div>
      
      
      
      
      
      <div id="menu">
      
          
       <ul>
           <li>    <a href="index.html">Úvod</a>   </li>          
           <li>   <a href="../../L2/web/index1.html" target="_blank">L2</a>    </li>
           <li>    <a href="http://spsostrov.cz" target="_blank"> SPŠ Ostrov</a> </li>
           <li> <a href="opakovani.html">Opakování </a>    </li> 
           <li>  <a href="galeriemini.html">Galerie</a>    </li>
           <li>   <a href="jquery.html">jQuery</a></li>
           <li>   <a href="http://www.jakpsatweb.cz/" target="_blank"> Jak psát web </a>    </li>    
           <li>  <a href="http://www.w3schools.com/" target="_blank"> W3     Schools</a> </li> 
           <li><a href="http://validator.w3.org/" target="_blank">HTML Validator</a>   </li>
         <li>  <a href="http://jigsaw.w3.org/css-validator/" target="_blank">CSS Validator</a> </li>
        <li>    <a href="galeriej.html">Jquery-galerie</a>   </li> 
        <li> <a href="01_php.php">PHP</a>  </li>
       </ul> 
      
      
      
      </div>
     
     
     
     
      <div id="obsah">
           <a href="02_php.php">PHP funkce</a>
          <?php
             
              //výpis textu
              echo "<h2>PHP začátky</h2>";
              echo "<p>Zobrazení uvozovek \"text\"</p>"; 
               
    
              //proměnné
              $jmeno ="Karel";
              $prijmeni="Veselý";
             
             
               //výpis názvu proměnné
                   echo '$jmeno <br>';
                   echo "Proměnná \$jmeno <br />";
                   
                // výpis hodnoty proměnné
               echo "Jmenuje te se: $jmeno <br />";
               echo $jmeno;
               echo "<br />" ;
               
               //řetězení
               echo $jmeno . " " . $prijmeni. "<br />";
               echo "Jmenujete se: ".$jmeno.
                     " a Vaše příjmeni je: ".$prijmeni. "<br />";
                     
                     
               // spojení
               $uzivatel=$jmeno." ".$prijmeni;
               echo $uzivatel;
                   
                echo "<hr />";
                
                
                
                // procvičení
                $a="0.6";
                $b="0";
                 //výpočty
                $soucet=$a + $b;
                $rozdil=$a - $b;
                $soucin=$a * $b;
          
                
                echo "a = ".$a. " ";
                echo "b = ".$b."<br />";
                echo "a+b= ".$soucet."<br />";
                 echo "a-b= ".$rozdil."<br />";
                echo "a*b= ".$soucin."<br />";
                // podmínka pro dělení nulou
                if($b=="0")
                 {
                  echo "nelze dělit nulou";
                 }
                else 
                {
                    $podil=$a/$b;  
                     echo "a/b= ".$podil."<br />";
                 }
               
                
                   
                     ?>
               
               
                <?php 
                
                //výpis čísel od 1 do 10  pevně dané hodnoty
                echo "<h2> Cyklus s FOR (1 2 3 ....) </h2>";
                        for($i=1; $i<=10 ; $i++)
                        
                        
                        {
                         echo "$i";
                       
                        
                        }
                echo "<hr />";
             
             
              //cyklus while -s podmínkou na začátku
            echo "<h2>Cyklus s WHILE</h2>";
              $pocitadlo=12;
              while($pocitadlo>10)
               {
                echo "<h3>Hezký den</h3> <br />";
                $pocitadlo --;
               
               
               
               
               }
               echo "<hr />";
              // cyklus do-while- s podmínkou na konci 
             
               echo "<h2>Cyklus s podmínkou  na konci DO-WHILE</h2>";
              $x=10;
              do
              {
            
              
                
                if($x==8)
                {
                echo "<h3> Den  </h3>";
                }
               
               
                else 
                
              {
                echo "<h3 id= 'do'> Den hezký </h3>"; 
              }
                $x-=2; //$x =x-2; je to totéž
              
              }
              
              while($x>0);
          
              // s každým průchodem cyklu zvětší písmena
           
               echo "<hr />";
                echo "<h2>Cyklus se zvětšením písmen</h2>"; 
                
                    for($y=1; $y<8 ; $y++)
                        
                        
                        {
                         echo "<font size=$y> Petr </font>";

                       
                        
                        }
                        
              
               echo "<hr />";
              // tabulka pomocí cyklu
               echo "<h2>Tabulka</h2>"; 
          echo "<table width='400px', height='200px', border='1', margin-left='100px'>";
           
          for($radek=1; $radek <6; $radek++){
        
          echo "<tr>";
         
          for($sloupec=1;$sloupec<5; $sloupec++)
          {
           echo "<td>$radek,$sloupec</td> ";
           
          }
          echo "</tr>";
          
          
          
            }
           echo "</table>";
           echo "<hr />";
             
              // tabulka násobení
   
          echo "<h2>Tabulka malá násobilka</h2>"; 
          echo "<table width='400px', height='200px', border='1', margin-left='100px'>";
          for ($t=0; $t <11; $t++)
          {
             echo "<th> $t </th>";  
          
          } 
          
          for($t=1; $t <11; $t++){
         
        
        
          echo "<tr>";
           echo "<th>$t</th>";
         
          for($r=1;$r<11; $r++)
         
         
          {
    
           echo "<td>".$t*$r."</td>" ;
           
          }
          echo "</tr>";
          
          
          
            }
           echo "</table>"; 
           
            echo "<hr />";
            //Nadpis pomocí print
                            
           echo "<h2> Zvětšení nadpisu</h2>";
            for($velikost=1; $velikost<7; $velikost++)
            {
               print "<h $velikost> SPŠ Ostrov </h $velikost>";
            
            }
          
        
         
               
               
               
               
               
               
               
               
               
               
               
               
               
               
               
               
               
               
               
               
               
               
               
               
               
               
               
               
               
               
               
               
               
               
               
               
                ?> 
             
             
             
             
             
             
             
             
             
             
             
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
      </div>
 
    
      
      
      
      
      
      <div id="paticka">
     
         <p id="copy">   &copy Petr Třmínek   17.9.2013       </p>
     
     
      </div>
     
     
     
    
    
 
                                                                
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
        </div>
 
 
   
 </body>
</html>