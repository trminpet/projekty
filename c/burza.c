#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
int compare(const void* a, const void* b)
{

    return (*(int*)b - *(int*)a);
}

int main()
{
    int maxID = 0, ID = 0, count = 0;
    char type = 'x';
    int n = 40;
    char point;

    printf("Nejvyssi ID:\n");

    if (scanf("%d", &maxID) == 1 && maxID > 0) {

        int** IDarray = (int**)malloc((maxID + 1) * sizeof(int*));
        int* SizArray = (int*)malloc((maxID + 1) * sizeof(int*));

        printf("Nabidka a poptavka:\n");
        //prodej
        while (!feof(stdin)) {
            if (scanf(" %c %d %d", &type, &ID, &count) == 3 && ID >= 0 && count > 0) {

                if (type == '+') {

                    if (ID <= maxID) {

                        n = SizArray[ID];
                        SizArray[ID] += 1;

                        if (IDarray[ID] == NULL) {

                            IDarray[ID] = (int*)malloc(n * sizeof(int*));
                            IDarray[ID][0] = count;
                        }

                        else {

                            if (SizArray[ID] < 19) {
                                //IDarray[ID]=(int *)realloc(IDarray[ID],n*sizeof(int*));
                                IDarray[ID][n] = count;
                            }

                            else {
                                IDarray[ID] = (int*)realloc(IDarray[ID], (2 * n) * sizeof(int*));
                                IDarray[ID][n] = count;
                            }
                        }
                    }

                    else {

                        printf("Nespravny vstup.\n");
                        free(SizArray);
                        return 0;
                    }
                }

                else if (type == '-') {

                    if (IDarray[ID] == NULL) {

                        printf("Neni k dispozici.\n");
                    }

                    else {

                        int i = 0;

                        int min = count;
                        int index;
                        int check = 0;
                        n = SizArray[ID];
                        for (i = 0; i < n; i++) {
                            //printf("%d",IDarray[ID][i]);

                            if (min > IDarray[ID][i] && IDarray[ID][i] != 0) {
                                min = IDarray[ID][i];
                                index = i;
                                check = 1;
                            }
                            else if (min == IDarray[ID][i] && IDarray[ID][i] != 0) {
                                min = IDarray[ID][i];
                                index = i;
                                check = 1;
                            }
                        }

                        if (check == 1) {
                            printf("Prodano za %d\n", IDarray[ID][index]);

                            IDarray[ID][index] = 0;
                        }

                        else {

                            printf("Neni k dispozici.\n");
                        }
                    }
                }

                else {
                    printf("Nespravny vstup.\n");

                    free(SizArray);

                    free(IDarray);
                    return 0;
                }
            }

            else {
                if (!feof(stdin)) {
                    printf("Nespravny vstup.\n");
                }

                free(SizArray);

                free(IDarray);

                return 0;
            }
        }

        free(IDarray);
        free(SizArray);
        return 0;
    }
    else {
        printf("Nespravny vstup.\n");

        return 0;
    }

    return 0;
}
