#include <stdio.h>
#include <stdlib.h>
#define SAR 250000
#include <string.h>

int check(char* copy, char* places, int n)
{

    int i = 0;
    for (i = 0; i <= n; i++) {

        if (copy[i] == places[i])
            continue;
        else
            return 0;
    }

    return 1;
}
int sickCounter(char* places, int n)
{

    int i = 0;
    int sick = 0;
    for (i = 0; i <= n; i++) {

        if (places[i] == '!')
            sick++;
    }
    return sick;
}
int nobodySick(char* places, int n)
{

    int i = 0;
    for (i = 0; i <= n; i++) {

        if (places[i] == '!')
            return 0;
    }
    return 1;
}

int main()
{

    int rows = 0, columns = 0;

    char places[SAR];
    char copy[SAR];
    char c = 'x';
    int b = 0, a = 0;
    int duration = 0;

    printf("Velikost kancelare:\n");
    if (scanf("%d %d", &rows, &columns) == 2 && rows >= 1 && rows <= 500 && columns >= 1 && columns <= 500) {
        int size = rows * columns - 1;
        c = fgetc(stdin);
        if (c == '\n') {
            int i = 0;
            int checkC = -1;

            int NL = 1;
            while ((places[i] = fgetc(stdin)) != EOF) {
                if (places[i] == '!' || places[i] == '.' || places[i] == 'o' || places[i] == '\n') {
                    checkC++;
                    if (places[i] == '\n') {
                        i = i;
                        NL++;
                        if (checkC != columns) {
                            printf("Nespravny vstup.\n");
                            return 0;
                        }
                        else {
                            checkC = -1;
                        }
                    }
                    else

                        i++;
                }
                else {
                    printf("Nespravny vstup.\n");

                    return 0;
                }
            }
            if (NL != rows + 1) {

                printf("Nespravny vstup.\n");
                return 0;
            }

            do {

                strncpy(copy, places, SAR);

                duration++;

                b = 0;

                for (b = 0; b <= size; b++) {

                    if (places[b] == '!') {

                        if (b % columns == 0) {

                            if (places[b + 1] == 'o') {
                                places[b + 1] = 'x';
                            }

                            if (places[b - columns] == 'o') {
                                places[b - columns] = 'x';
                            }

                            if (places[b + columns] == 'o') {
                                places[b + columns] = 'x';
                            }
                        }
                        else if (b % columns == columns - 1) {

                            if (places[b - 1] == 'o') {
                                places[b - 1] = 'x';
                            }

                            if (places[b - columns] == 'o') {
                                places[b - columns] = 'x';
                            }

                            if (places[b + columns] == 'o') {
                                places[b + columns] = 'x';
                            }
                        }

                        else {

                            if (places[b - 1] == 'o') {
                                places[b - 1] = 'x';
                            }

                            if (places[b + 1] == 'o') {
                                places[b + 1] = 'x';
                            }

                            if (places[b - columns] == 'o') {
                                places[b - columns] = 'x';
                            }

                            if (places[b + columns] == 'o') {
                                places[b + columns] = 'x';
                            }
                        }
                    }
                }

                for (a = 0; a <= size; a++) {

                    if (places[a] == 'x') {
                        places[a] = '!';
                    }
                }

            } while (check(copy, places, size) == 0);

            int a = sickCounter(places, size);

            if (nobodySick(places, size) == 1) {

                printf("Nikdo neonemocnel.\n");
            }
            else {
                printf("Nakazenych: %d, doba sireni nakazy: %d\n", a, duration - 1);
            }
        }
    }

    else
        printf("Nespravny vstup.\n");

    return 0;
}
