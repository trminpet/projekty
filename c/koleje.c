/* 
 * File:   main.c
 * Author: trminpet
 *
 * Created on November 7, 2015, 1:04 PM
 */

#include <stdio.h>
#include <stdlib.h>

/*
 * 
 */

int main()
{
    long long lenght1, lenght2, distance;
    char type;
    long long max, min, varitation = 0;

    printf("Delky koleji:\n");
    if (scanf("%Ld %Ld", &lenght1, &lenght2) == 2 && lenght1 > 0 && lenght2 > 0 && lenght1 != lenght2) {
        printf("Vzdalenost:\n");
        if (scanf(" %c %Ld", &type, &distance) == 2 && distance >= 0 && (type == '+' || type == '-')) {
            if (type == '+') {

                long i, k;
                int combination = 0;
                for (i = -1; i <= distance / lenght1; i++) {
                    for (k = distance / lenght2; k >= 0; k--) {

                        if (i * lenght1 + k * lenght2 == distance) {
                            printf("= %li * %li + %li * %li\n", lenght1, i, lenght2, k);
                            combination++;
                        }
                    }
                }
                if (combination == 0) {
                    printf("Reseni neexistuje.\n");
                }
                else {
                    printf("Celkem variant: %d\n", combination);
                }
            }
            else {

                if (lenght1 > lenght2) {

                    max = lenght1;
                    min = lenght2;
                }
                else {
                    max = lenght2;
                    min = lenght1;
                }

                long a;

                for (a = distance / max; a >= 0; a--) {

                    if ((distance - max * a) % min == 0) {

                        varitation++;
                    }
                }

                if (varitation != 0) {
                    printf("Celkem variant: %d\n", varitation);
                }
                else {
                    printf("Reseni neexistuje.\n");
                }
            }
        }

        else {
            printf("Nespravny vstup.\n");
        }
    }

    else {
        printf("Nespravny vstup.\n");
    }
    return (EXIT_SUCCESS);
}

