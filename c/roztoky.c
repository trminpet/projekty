/* 
 * File:   main.c
 * Author: trminpet
 *
 * Created on October 31, 2015, 10:28 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
/*
 * 
 */ double mnozstviA, koncentraceA, mnozstviB, koncentraceB, mnozstviC, koncentraceC, res;
void first()
{
    printf("Hmotnost a koncentrace #1:\n");
}
void second()
{
    printf("Hmotnost a koncentrace #2:\n");
}
void third()
{
    printf("Hmotnost a koncentrace vysledku:\n");
}
void final()
{

    double vys = mnozstviC * koncentraceC;
    double vypocet = (vys - (koncentraceA * mnozstviC)) / (-koncentraceA + koncentraceB);
    double vypocetDva = mnozstviC - vypocet;
    if (vypocetDva <= mnozstviA * 1.000000001 && vypocet <= mnozstviB * 1.000000001) {

        if (((koncentraceA + koncentraceB) - (2 * koncentraceC)) == 0 && mnozstviA + mnozstviB == mnozstviC) {

            printf("0.000000 x #1 + 0.000000 x #2\n");
        }
        else if (mnozstviA + mnozstviB == mnozstviC) {

            printf("%f x #1 + %f x #2\n", mnozstviA, mnozstviB);
        }

        else if (mnozstviC == 0 && koncentraceC >= 0) {

            printf("0.000000 x #1 + 0.000000 x #2\n");
        }
        else {

            printf("%f x #1 + %f x #2\n", vypocetDva, vypocet);
        }
    }
    else if (koncentraceA == koncentraceB && koncentraceB == koncentraceA && mnozstviA + mnozstviB == mnozstviC) {

        printf("%f x #1 + %f x #2\n", mnozstviA, mnozstviB);
    }
    else if (koncentraceA == koncentraceB && koncentraceB == koncentraceA && mnozstviC > mnozstviA) {

        printf("%f x #1 + %f x #2\n", mnozstviA, mnozstviC - mnozstviA);
    }

    else if (koncentraceA == koncentraceB && koncentraceB == koncentraceA && mnozstviC < mnozstviA) {

        printf("%f x #1 + 0.000000 x #2\n", mnozstviC);
    }

    else {

        printf("Nelze dosahnout.\n");
        //  printf("%f x #1 + %f x #2\n", vypocetDva,vypocet);
    }
}

int main()
{

    first();
    if (scanf("%lf", &mnozstviA) == 1 && scanf("%lf", &koncentraceA) == 1 && mnozstviA >= 0 && koncentraceA >= 0 && koncentraceA <= 1) {

        second();
        if (scanf("%lf", &mnozstviB) == 1 && scanf("%lf", &koncentraceB) == 1 && mnozstviB >= 0 && koncentraceB >= 0 && koncentraceB <= 1) {

            third();
            if (scanf("%lf", &mnozstviC) == 1 && scanf("%lf", &koncentraceC) == 1 && mnozstviC >= 0 && koncentraceC >= 0 && koncentraceC <= 1) {
                final();
            }
            else {
                printf("Nespravny vstup.\n");
                return 1;
            }
        }

        else {
            printf("Nespravny vstup.\n");
            return 1;
        }
    }
    else {
        printf("Nespravny vstup.\n");
        return 1;
    }

    return (EXIT_SUCCESS);
}

