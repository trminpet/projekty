#ifndef __PROGTEST__
#include <stdio.h>
#include <assert.h>
#include <math.h>
#endif /* __PROGTEST__ */

int LeapYearCheck(int i)
{
    if (i >= 4000) {
        //prestupny rok
        if (i % 4 == 0 && (i % 100 != 0 || i % 400 == 0) && i % 4000 != 0) {
            return 1;
        }
        //neprestupny rok
        else {
            return 0;
        }
    }
    //prestupny rok
    else if (i % 4 == 0 && (i % 100 != 0 || i % 400 == 0)) {
        return 1;
    }
    //neprestupnz rok
    else {
        return 0;
    }
}

int DaysOfMonth(int year, int month)
{
    int days = 0;

    switch (month) {
    case 1:
        days = 31;
        break;
    case 2: {
        if (LeapYearCheck(year) == 1) {
            days = 29;
        }
        else
            days = 28;
    } break;
    case 3:
        days = 31;
        break;
    case 4:
        days = 30;
        break;
    case 5:
        days = 31;
        break;
    case 6:
        days = 30;
        break;
    case 7:
        days = 31;
        break;
    case 8:
        days = 31;
        break;
    case 9:
        days = 30;
        break;
    case 10:
        days = 31;
        break;
    case 11:
        days = 30;
        break;
    case 12:
        days = 31;
        break;
    }

    return days;
}

int MonthCounting(int year, int month)
{
    int days = 0;

    switch (month) {
    case 1:
        days = 0;
        break;
    case 2:
        days = 31;
        break;
    case 3:
        days = 59;
        break;
    case 4:
        days = 90;
        break;
    case 5:
        days = 120;
        break;
    case 6:
        days = 151;
        break;
    case 7:
        days = 181;
        break;
    case 8:
        days = 212;
        break;
    case 9:
        days = 242;
        break;
    case 10:
        days = 273;
        break;
    case 11:
        days = 303;
        break;
    case 12:
        days = 334;
        break;
    }

    return days;
}
int DiffenceBetweenDates(int y, int m, int d)
{

    int startY = 1900;

    int i;
    int days = 0;

    for (i = startY; i < y; i++) {

        //prestupny rok
        if (LeapYearCheck(i)) {
            days += 366;
        }
        //neprestupny rok
        else {
            days += 365;
        }
    }
    if (LeapYearCheck(y) == 1) {

        days += fabs(1 - d) + fabs(MonthCounting(y, m));
    }
    else
        days += fabs(1 - d) + fabs(MonthCounting(y, m));

    //printf("%d ",days);

    return days;
}

int CorrectDate(int y1, int m1, int d1,
    int y2, int m2, int d2)
{
    int check = 0;

    if (y1 >= 1900 && y2 >= 1900 && (m1 >= 1 && m1 <= 12) && (m2 >= 1 && m2 <= 12) && (d1 >= 1 && d1 <= DaysOfMonth(y1, m1)) && (d2 >= 1 && d2 <= DaysOfMonth(y2, m2))) {
        if (DiffenceBetweenDates(y1, m1, d1) <= DiffenceBetweenDates(y2, m2, d2)) {
            check = 1;
        }
    }

    else
        check = 0;

    return check;
}

int CountFriday13(int y1, int m1, int d1,
    int y2, int m2, int d2, long long int* cnt)
{

    if (CorrectDate(y1, m1, d1, y2, m2, d2) == 1) {

        int i;
        int a;
        int b;
        int counter = 0;
        int counterOne = 0;
        int counterTwo = 0;

        int dateOne = DiffenceBetweenDates(y1, m1, d1);
        int dateTwo = DiffenceBetweenDates(y2, m2, d2);
        //rozdil od 1.1900

        a = 1900;
        b = 0;

        for (i = 1; i <= dateOne; i += DaysOfMonth(a, b)) {

            if (b == 12) {

                a += 1;
                b = 0;
            }
            b++;
            if (i % 7 == 0) {

                counterOne++;
            }
        }

        a = 1900;
        b = 0;
        for (i = 1; i <= dateTwo; i += DaysOfMonth(a, b)) {

            if (b == 12) {

                a += 1;
                b = 0;
            }
            b++;
            if (i % 7 == 0) {

                counterTwo++;
            }
        }
        counter = counterTwo - counterOne;
        if (counter <= 0) {
            counter = 0;
            *cnt = counter;
            return 1;
        }
        if ((dateOne + 1) % 7 == 5 && d1 == 13 && d2 == 13)
            counter--;
        if ((dateOne + 1) % 7 == 5 && d1 == 13 && d2 != 13)
            counter++;
        if ((dateOne + 1) % 7 == 5 && d1 == 13 && d2 == 13 && (dateTwo + 1) % 7 != 5)
            counter += 2;
        if ((dateOne + 1) % 7 == 5 && d1 == 13 && d2 == 13 && (dateTwo + 1) % 7 == 5)
            counter += 2;
        *cnt = counter;

        return 1;
    }

    else
        return 0;
}
#ifndef __PROGTEST__
int main(int argc, char* argv[])
{
    long long int cnt;

    assert(CountFriday13(1900, 1, 1,
               2015, 1, 1, &cnt)
            == 1
        && cnt == 197LL);
    assert(CountFriday13(1900, 1, 1,
               2015, 2, 1, &cnt)
            == 1
        && cnt == 197LL);
    assert(CountFriday13(1900, 1, 1,
               2015, 1, 13, &cnt)
            == 1
        && cnt == 197LL);
    assert(CountFriday13(1900, 1, 1,
               2015, 2, 13, &cnt)
            == 1
        && cnt == 198LL);
    assert(CountFriday13(1904, 1, 1,
               2015, 1, 1, &cnt)
            == 1
        && cnt == 189LL);
    assert(CountFriday13(1904, 1, 1,
               2015, 2, 1, &cnt)
            == 1
        && cnt == 189LL);
    assert(CountFriday13(1904, 1, 1,
               2015, 1, 13, &cnt)
            == 1
        && cnt == 189LL);
    assert(CountFriday13(1904, 1, 1,
               2015, 2, 13, &cnt)
            == 1
        && cnt == 190LL);

    assert(CountFriday13(1905, 2, 13,
               2015, 1, 1, &cnt)
            == 1
        && cnt == 187LL);
    assert(CountFriday13(1905, 2, 13,
               2015, 2, 1, &cnt)
            == 1
        && cnt == 187LL);
    assert(CountFriday13(1905, 2, 13,
               2015, 1, 13, &cnt)
            == 1
        && cnt == 187LL);
    assert(CountFriday13(1905, 2, 13,
               2015, 2, 13, &cnt)
            == 1
        && cnt == 188LL);
    assert(CountFriday13(1905, 1, 13,
               2015, 1, 1, &cnt)
            == 1
        && cnt == 188LL);
    assert(CountFriday13(1905, 1, 13,
               2015, 2, 1, &cnt)
            == 1
        && cnt == 188LL);
    assert(CountFriday13(1905, 1, 13,
               2015, 1, 13, &cnt)
            == 1
        && cnt == 188LL);
    assert(CountFriday13(1905, 1, 13,
               2015, 2, 13, &cnt)
            == 1
        && cnt == 189LL);
    assert(CountFriday13(2015, 11, 1,
               2015, 10, 1, &cnt)
        == 0);
    assert(CountFriday13(2015, 10, 32,
               2015, 11, 10, &cnt)
        == 0);
    assert(CountFriday13(2090, 2, 29,
               2090, 2, 29, &cnt)
        == 0);
    assert(CountFriday13(2096, 2, 29,
               2096, 2, 29, &cnt)
            == 1
        && cnt == 0LL);
    assert(CountFriday13(2100, 2, 29,
               2100, 2, 29, &cnt)
        == 0);
    assert(CountFriday13(2000, 2, 29,
               2000, 2, 29, &cnt)
            == 1
        && cnt == 0LL);

    return 0;
}
#endif /* __PROGTEST__ */
