
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
 <head>
  <meta name="generator" content="PSPad editor, www.pspad.com" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
   <link href="styl.css" rel="stylesheet" type="text/css" />
  <title>PHP stránky</title>
  </head>
  <body>
  
  <div id="hlavicka">
   
   <?php include 'hlavicka.php'; ?>
 
  </div>
   
   <div id="menu">
   <?php include 'menu.php'; ?>
   
   </div>
  
   <div id="obsah">
    <?php
             
              //výpis textu
              echo "<h2>PHP začátky</h2>";
              echo "<p>Zobrazení uvozovek \"text\"</p>"; 
               
    
              //proměnné
              $jmeno ="Karel";
              $prijmeni="Veselý";
             
             
               //výpis názvu proměnné
                   echo '$jmeno <br>';
                   echo "Proměnná \$jmeno <br />";
                   
                // výpis hodnoty proměnné
               echo "Jmenuje te se: $jmeno <br />";
               echo $jmeno;
               echo "<br />" ;
               
               //řetězení
               echo $jmeno . " " . $prijmeni. "<br />";
               echo "Jmenujete se: ".$jmeno.
                     " a Vaše příjmeni je: ".$prijmeni. "<br />";
                     
                     
               // spojení
               $uzivatel=$jmeno." ".$prijmeni;
               echo $uzivatel;
                   
                echo "<hr />";
                
                
                
                // procvičení
                $a="0.6";
                $b="0";
                 //výpočty
                $soucet=$a + $b;
                $rozdil=$a - $b;
                $soucin=$a * $b;
          
                
                echo "a = ".$a. " ";
                echo "b = ".$b."<br />";
                echo "a+b= ".$soucet."<br />";
                 echo "a-b= ".$rozdil."<br />";
                echo "a*b= ".$soucin."<br />";
                // podmínka pro dělení nulou
                if($b=="0")
                 {
                  echo "nelze dělit nulou";
                 }
                else 
                {
                    $podil=$a/$b;  
                     echo "a/b= ".$podil."<br />";
                 }
               
                
                   
                     ?>
               
               
                <?php 
                
                //výpis čísel od 1 do 10  pevně dané hodnoty
                echo "<h2> Cyklus s FOR (1 2 3 ....) </h2>";
                        for($i=1; $i<=10 ; $i++)
                        
                        
                        {
                         echo "$i";
                       
                        
                        }
                echo "<hr />";
             
             
              //cyklus while -s podmínkou na začátku
            echo "<h2>Cyklus s WHILE</h2>";
              $pocitadlo=12;
              while($pocitadlo>10)
               {
                echo "<h3>Hezký den</h3> <br />";
                $pocitadlo --;
               
               
               
               
               }
               echo "<hr />";
              // cyklus do-while- s podmínkou na konci 
             
               echo "<h2>Cyklus s podmínkou  na konci DO-WHILE</h2>";
              $x=10;
              do
              {
            
              
                
                if($x==8)
                {
                echo "<h3> Den  </h3>";
                }
               
               
                else 
                
              {
                echo "<h3 id= 'do'> Den hezký </h3>"; 
              }
                $x-=2; //$x =x-2; je to totéž
              
              }
              
              while($x>0);
          
              // s každým průchodem cyklu zvětší písmena
           
               echo "<hr />";
                echo "<h2>Cyklus se zvětšením písmen</h2>"; 
                
                    for($y=1; $y<8 ; $y++)
                        
                        
                        {
                         echo "<font size=$y> Petr </font>";

                       
                        
                        }
                        
              
               echo "<hr />";
              // tabulka pomocí cyklu
               echo "<h2>Tabulka</h2>"; 
          echo "<table width='400px', height='200px', border='1', margin-left='100px'>";
           
          for($radek=1; $radek <6; $radek++){
        
          echo "<tr>";
         
          for($sloupec=1;$sloupec<5; $sloupec++)
          {
           echo "<td>$radek,$sloupec</td> ";
           
          }
          echo "</tr>";
          
          
          
            }
           echo "</table>";
           echo "<hr />";
             
              // tabulka násobení
   
          echo "<h2>Tabulka malá násobilka</h2>"; 
          echo "<table width='400px', height='200px', border='1', margin-left='100px'>";
          for ($t=0; $t <11; $t++)
          {
             echo "<th> $t </th>";  
          
          } 
          
          for($t=1; $t <11; $t++){
         
        
        
          echo "<tr>";
           echo "<th>$t</th>";
         
          for($r=1;$r<11; $r++)
         
         
          {
    
           echo "<td>".$t*$r."</td>" ;
           
          }
          echo "</tr>";
          
          
          
            }
           echo "</table>"; 
           
            echo "<hr />";
            //Nadpis pomocí print
                            
           echo "<h2> Zvětšení nadpisu</h2>";
            for($velikost=1; $velikost<7; $velikost++)
            {
               print "<h $velikost> SPŠ Ostrov </h $velikost>";
            
            }
          
   ?> 
             
   </div>
   
   <div id="paticka">
   <?php include 'paticka.php'; ?>
  </div>



  </body>
  </html>
